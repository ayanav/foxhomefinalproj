package Tests;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

import Identifications.NewMenuIds;
import Infra.ExtentReportInfra;

public class NewMenuTest {

	public static WebDriver driver;
	public static NewMenuIds pof;
	
	static ExtentReports extent;
	static ExtentTest newMenu;
	
	static ExtentReportInfra exm = new ExtentReportInfra(driver);
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		extent = exm.GetExtent();
		newMenu = exm.createTest("NewMenu", "NewMenu");
		
		System.setProperty("webdriver.edge.driver","C:\\ayana\\webdriver\\msedgedriver.exe"); 
	    
		driver = new EdgeDriver();
	    driver.manage().window().maximize();
	    
		driver.get("https://www.foxhome.co.il/");
		Thread.sleep(1000);
		
		pof = new NewMenuIds();
		pof = PageFactory.initElements(driver, NewMenuIds.class);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		extent.flush();
		driver.quit();
	}

	@Test
	// Checking the first item in the New! menus
	public void test_01() {
		try
		{
			pof.NewMenu.click();
		
			Thread.sleep(1000);

			 if (pof.Title.getText().equals("חדש!")) {
				 newMenu.pass("New!");	
			}
			 else {
				 newMenu.fail("New!", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			newMenu.fail("New!");
		}
	}

}
