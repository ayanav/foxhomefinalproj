package Tests;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;

import Identifications.TableHostingMenuIds;
import Infra.ExtentReportInfra;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TableHostingMenuTest {
	
	public static WebDriver driver;
	public static TableHostingMenuIds pof;
	
	static ExtentReports extent;
	static ExtentTest TableHostingMenu;
	
	static ExtentReportInfra exm = new ExtentReportInfra(driver);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		extent = exm.GetExtent();
		TableHostingMenu = exm.createTest("TableHostingMenu", "TableAndHostingMenu");
		
		System.setProperty("webdriver.edge.driver","C:\\ayana\\webdriver\\msedgedriver.exe"); 
	    
		driver = new EdgeDriver();
	    driver.manage().window().maximize();
	    
		driver.get("https://www.foxhome.co.il/");
		Thread.sleep(1000);
		
		pof = new TableHostingMenuIds();
		pof = PageFactory.initElements(driver, TableHostingMenuIds.class);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		extent.flush();
		driver.quit();
	}

//	@Ignore
	@Test
	public void test_01_All_Products() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			pof.MainItems_AllProducts.get(0).click();
		
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("שולחן ואירוח")) {
				 TableHostingMenu.pass("Table and Hosting - All products");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - All products", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - All products");
		}
	}
	
//	@Ignore
	@Test
	public void test_02_WhiteSeries() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.MainItems_SecondGroup.get(0).click();
			pof.WhiteSeries.click();
		
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("הסדרה הלבנה")) {
				 TableHostingMenu.pass("Table and Hosting - White series");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - White series", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - White series");
		}
	}
	
//	@Ignore
	@Test
	public void test_03_Serving() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.MainItems_FirstGroup.get(2).click();
			pof.Serving.click();
		
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("הגשה")) {
				 TableHostingMenu.pass("Table and Hosting - Serving");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Serving", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Serving");
		}
	}
	
//	@Ignore
	@Test
	public void test_04_Bowls() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(4).click();
			pof.Bowls.click();
		
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("קערות וקעריות")) {
				 TableHostingMenu.pass("Table and Hosting - Bowls and Small Bowls");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Bowls and Small Bowls", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Bowls and Small Bowls");
		}
	}
	
//	@Ignore
	@Test
	public void test_05_ServingPlates() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			// doesn't work well... Need to check how to find specific element using link text... perhaps in a loop
			//pof.SubMenuItems.get(5).click();
			pof.ServingPlates.click();
		
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("צלחות הגשה")) {
				 TableHostingMenu.pass("Table and Hosting - Serving Plates");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Serving Plates", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Serving Plates");
		}
	}
	
//	@Ignore
	@Test
	public void test_06_ServingSpoons() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.ServingSpoons.click();
			
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("כפות הגשה")) {
				 TableHostingMenu.pass("Table and Hosting - Serving Spoons");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Serving Spoons", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Serving Spoons");
		}
	}
	
//	@Ignore
	@Test
	public void test_07_Plates() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.Plates.click();
			
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("צלחות")) {
				 TableHostingMenu.pass("Table and Hosting - Plates");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Plates", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Plates");
		}
	}

//	@Ignore
	@Test
	public void test_08_DishesSeries() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.DishesSeries.click();
			
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("סדרות כלים")) {
				 TableHostingMenu.pass("Table and Hosting - Dishes Series");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Dishes Series", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Dishes Series");
		}
	}
	
//	@Ignore
	@Test
	public void test_09_MelaminePlates() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.MelaminePlates.click();
			
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("צלחות מלמין")) {
				 TableHostingMenu.pass("Table and Hosting - Melamine Plates");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Melamine Plates", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Melamine Plates");
		}
	}
	
//	@Ignore
	@Test
	public void test_10_GlassesJugs() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.GlassesJugs.click();
			
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("כוסות וקנקנים")) {
				 TableHostingMenu.pass("Table and Hosting - Glasses and Jugs");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Glasses and Jugs", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Glasses and Jugs");
		}
	}
	
//	@Ignore
	@Test
	public void test_11_WaterGlasses() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.WaterGlasses.click();
			
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("כוסות מים ושתייה קלה")) {
				 TableHostingMenu.pass("Table and Hosting - Water and Soft Drink Glasses");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Water and Soft Drink Glasses", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Water and Soft Drink Glasses");
		}
	}
	
//	@Ignore
	@Test
	public void test_12_Mugs() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.Mugs.click();
			
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("ספלים ומאגים")) {
				 TableHostingMenu.pass("Table and Hosting - Mugs");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Mugs", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Mugs");
		}
	}
	
//	@Ignore
	@Test
	public void test_13_WineAlcohol() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.WineAlcohol.click();
			
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("יין ואלכוהול")) {
				 TableHostingMenu.pass("Table and Hosting - Wine and Alcohol");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Wine and Alcohol", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Wine and Alcohols");
		}
	}
	
//	@Ignore
	@Test
	public void test_14_JugsDispensers() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.JugsDispensers.click();
			
			Thread.sleep(2000);

		    if (pof.Title.getText().equals("קנקנים ודיספנסרים")) {
				 TableHostingMenu.pass("Table and Hosting - Jugs and Dispensers");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Jugs and Dispensers", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Jugs and Dispensers");
		}
	}
	
	@Test
	public void test_15_Flatware() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.Flatware.click();
			
			Thread.sleep(2000);

			// Define the string which contains quotation marks
			String pageTitle = "סכו" + "\"" + "ם";
			
		    if (pof.Title.getText().equals(pageTitle)) {
				 TableHostingMenu.pass("Table and Hosting - Flatware");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Flatware", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Flatware");
		}
	}
	
	@Test
	public void test_16_FlatwareSet() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.FlatwareSet.click();
			
			Thread.sleep(2000);

			// Define the string which contains quotation marks
			String pageTitle = "סט סכו" + "\"" + "ם";
			
		    if (pof.Title.getText().equals(pageTitle)) {
				 TableHostingMenu.pass("Table and Hosting - Flatware Set");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Flatware Set", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Flatware Set");
		}
	}
	
	@Test
	public void test_17_SingleFlatware() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.SingleFlatware.click();
			
			Thread.sleep(2000);
		
		    if (pof.Title.getText().equals("בדידים")) {
				 TableHostingMenu.pass("Table and Hosting - Single Flatware");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Single Flatware", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Single Flatware");
		}
	}
	
	@Test
	public void test_18_TableTextiles() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.TableTextiles.click();
			
			Thread.sleep(2000);
		
		    if (pof.Title.getText().equals("טקסטיל שולחן")) {
				 TableHostingMenu.pass("Table and Hosting - Table Textiles");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Table Textiles", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Table Textiles");
		}
	}
	
	@Test
	public void test_19_Tablecloths() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.Tablecloths.click();
			
			Thread.sleep(2000);
		
		    if (pof.Title.getText().equals("מפות שולחן")) {
				 TableHostingMenu.pass("Table and Hosting - Tablecloths");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Tablecloths", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Tablecloths");
		}
	}
	
	@Test
	public void test_20_TableRunners() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.TableRunners.click();
			
			Thread.sleep(2000);
		
		    if (pof.Title.getText().equals("ראנרים")) {
				 TableHostingMenu.pass("Table and Hosting - Table Runners");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Table Runners", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Table Runners");
		}
	}
	
	@Test
	public void test_21_Placemats() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.Placemats.click();
			
			Thread.sleep(2000);
		
		    if (pof.Title.getText().equals("פלייסמטים")) {
				 TableHostingMenu.pass("Table and Hosting - Placemats");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Placemats", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Placemats");
		}
	}
	
	@Test
	public void test_22_Napkins() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.Napkins.click();
			
			Thread.sleep(2000);
		
		    if (pof.Title.getText().equals("מפיות")) {
				 TableHostingMenu.pass("Table and Hosting - Napkins");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Napkins", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Napkins");
		}
	}
	
	@Test
	public void test_23_Banderoles() {
		try
		{
			Actions action = new Actions(driver);
			
			// Go to the main Table and hosting link in order to open the sub menu
			action.moveToElement(pof.TableHosting_main).perform();
			Thread.sleep(1000);
			
			//pof.SubMenuItems.get(6).click();
			pof.Banderoles.click();
			
			Thread.sleep(2000);
		
		    if (pof.Title.getText().equals("חבקים")) {
				 TableHostingMenu.pass("Table and Hosting - Banderoles");	
			}
			 else {
				 TableHostingMenu.fail("Table and Hosting - Banderoles", MediaEntityBuilder.createScreenCaptureFromPath(exm.CaptureScreen(driver)).build());
			}
		}
		catch (Exception e)
		{
			TableHostingMenu.fail("Table and Hosting - Banderoles");
		}
	}
}
