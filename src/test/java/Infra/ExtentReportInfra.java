package Infra;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import com.google.common.io.Files;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportInfra {
	
	public static WebDriver driver;
	// Constructor
	public ExtentReportInfra(WebDriver driver){
	this.driver=driver;
	}
	public static ExtentReports extent;
	static ExtentTest test;
	public static ExtentHtmlReporter htmlReporter;
	
	// Creating a folder with current date as the name
	static DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
	static Date today = Calendar.getInstance().getTime();
	static String reportDate = df.format(today);
	public static String filePath = "d:\\"+reportDate+"\\exReport.html";

	public static ExtentReports GetExtent()
	{
		new File("d:\\"+ reportDate).mkdirs();
		if (extent != null)
		return extent; //avoid creating new instance of html file
		extent = new ExtentReports();
		extent.attachReporter(getHtmlReporter());
		return extent;
	}

		// Function to create a new test
		public static ExtentTest createTest(String name, String description)
		{
			test = extent.createTest(name, description);
			return test;
		}
		private static ExtentHtmlReporter getHtmlReporter() 
		{
			htmlReporter = new ExtentHtmlReporter(filePath);
			htmlReporter.config().setDocumentTitle("QAV automation report");
			htmlReporter.config().setReportName("Regression cycle");
			
			htmlReporter.config().setEncoding("windows-1255");
			return htmlReporter;
		}
		
		// Capture a screen
		public static String CaptureScreen(WebDriver driver) throws AWTException, IOException
		{
			LocalDateTime now = LocalDateTime.now();
			String time =now.format(DateTimeFormatter.ofPattern("ddHHmmss"));
			String folderPath = ("d:\\"+reportDate);
			String imagePath = folderPath +"/pic" +time +".jpg";
			TakesScreenshot oScn = (TakesScreenshot) driver;
			File oScnShot = oScn.getScreenshotAs(OutputType.FILE);
			File oDest = new File(imagePath);
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			Files.copy(scrFile, oDest);
			return imagePath; 
		}
}
