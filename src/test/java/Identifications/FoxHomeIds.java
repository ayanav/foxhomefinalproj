package Identifications;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class FoxHomeIds {
	
	@FindBy (how = How.XPATH, using = "//span[@class='title'][contains(text(),'!')]")
	static public WebElement NewMenu;
	
	@FindBy (how = How.XPATH, using = "/html[1]/body[1]/div[3]/header[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/nav[1]/div[1]/ul[1]/li[2]/a[1]/span[1]/span[2]")
	static public WebElement TableHosting_main;
		
	// Get the title of the page
	@FindBy (how = How.XPATH, using = "//span[@class='page-heading-title-text']")
	static public WebElement Title;
	
	// First group of main menus in the sub-menus
	@FindBy(how = How.XPATH, using = "//li[@class='category-item level_1 parent']/a")
	public List<WebElement> MainItems_FirstGroup;
	
	// Second group of main menus in the sub-menu
	@FindBy(how = How.XPATH, using = "//ul[@class='level_1']/li[@class='category-item level_1']")
	public List<WebElement> MainItems_SecondGroup;
	
	// All products items from all menus
	@FindBy(how = How.XPATH, using = "//div[@class='page-header-navigation-dropdown-show_all']")
	public List<WebElement> MainItems_AllProducts;
	
	@FindBy(how = How.XPATH, using = "//li[@class='category-item level_2']")
	public List<WebElement> SubMenuItems;

	@FindBy (how = How.LINK_TEXT, using = "צלחות הגשה")
	static public WebElement ServingPlates;

	@FindBy (how = How.LINK_TEXT, using = "כפות הגשה")
	static public WebElement ServingSpoons;
}
