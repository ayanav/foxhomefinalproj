package Identifications;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class TableHostingMenuIds {
	
	@FindBy (how = How.XPATH, using = "/html[1]/body[1]/div[3]/header[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/nav[1]/div[1]/ul[1]/li[2]/a[1]/span[1]/span[2]")
	static public WebElement TableHosting_main;
		
	// Get the title of the page
	@FindBy (how = How.XPATH, using = "//span[@class='page-heading-title-text']")
	static public WebElement Title;
	
//	// First group of main menus in the sub-menus
//	@FindBy(how = How.XPATH, using = "//li[@class='category-item level_1 parent']/a")
//	public List<WebElement> MainItems_FirstGroup;
//	
//	// Second group of main menus in the sub-menu
//	@FindBy(how = How.XPATH, using = "//ul[@class='level_1']/li[@class='category-item level_1']")
//	public List<WebElement> MainItems_SecondGroup;
	
	// All products items from all menus
	@FindBy(how = How.XPATH, using = "//div[@class='page-header-navigation-dropdown-show_all']")
	public List<WebElement> MainItems_AllProducts;
	
//	@FindBy(how = How.XPATH, using = "//li[@class='category-item level_2']")
//	public List<WebElement> SubMenuItems;
	
	@FindBy (how = How.LINK_TEXT, using = "הסדרה הלבנה")
	static public WebElement WhiteSeries;
	
	@FindBy (how = How.LINK_TEXT, using = "הגשה")
	static public WebElement Serving;
	
	@FindBy (how = How.LINK_TEXT, using = "קערות וקעריות")
	static public WebElement Bowls;

	@FindBy (how = How.LINK_TEXT, using = "צלחות הגשה")
	static public WebElement ServingPlates;

	@FindBy (how = How.LINK_TEXT, using = "כפות הגשה")
	static public WebElement ServingSpoons;

	@FindBy (how = How.LINK_TEXT, using = "צלחות")
	static public WebElement Plates;
	
	@FindBy (how = How.LINK_TEXT, using = "סדרות כלים")
	static public WebElement DishesSeries;
	
	@FindBy (how = How.LINK_TEXT, using = "צלחות מלמין")
	static public WebElement MelaminePlates;
	
	@FindBy (how = How.LINK_TEXT, using = "כוסות וקנקנים")
	static public WebElement GlassesJugs;
	
	@FindBy (how = How.PARTIAL_LINK_TEXT, using = "כוסות מים")
	static public WebElement WaterGlasses;
	
	@FindBy (how = How.LINK_TEXT, using = "ספלים ומאגים")
	static public WebElement Mugs;
	
	@FindBy (how = How.LINK_TEXT, using = "יין ואלכוהול")
	static public WebElement WineAlcohol;
	
	@FindBy (how = How.LINK_TEXT, using = "קנקנים ודיספנסרים")
	static public WebElement JugsDispensers;
	
	@FindBy (how = How.LINK_TEXT, using = "סכו" + "\"" + "ם")
	static public WebElement Flatware;
	
	@FindBy (how = How.PARTIAL_LINK_TEXT, using = "סט סכו")
	static public WebElement FlatwareSet;
	
	@FindBy (how = How.LINK_TEXT, using = "בדידים")
	static public WebElement SingleFlatware;
	
	@FindBy (how = How.LINK_TEXT, using = "טקסטיל שולחן")
	static public WebElement TableTextiles;
	
	@FindBy (how = How.LINK_TEXT, using = "מפות שולחן")
	static public WebElement Tablecloths;
	
	@FindBy (how = How.LINK_TEXT, using = "ראנרים")
	static public WebElement TableRunners;
	
	@FindBy (how = How.LINK_TEXT, using = "פלייסמטים")
	static public WebElement Placemats;
	
	@FindBy (how = How.LINK_TEXT, using = "מפיות")
	static public WebElement Napkins;
	
	@FindBy (how = How.LINK_TEXT, using = "חבקים")
	static public WebElement Banderoles;
}
