package Identifications;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class NewMenuIds {
	
	@FindBy (how = How.XPATH, using = "//span[@class='title'][contains(text(),'!')]")
	static public WebElement NewMenu;
	
	@FindBy (how = How.XPATH, using = "//span[@class='page-heading-title-text']")
	static public WebElement Title;
	

}
